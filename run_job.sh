curl --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" \
       --form "variables[][key]=IMAGE_APP" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$IMAGE_APP" \
       --form "variables[][key]=IMAGE_NGINX" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$IMAGE_NGINX" \
       --form "variables[][key]=PORT" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$PORT" \
	"https://gitlab.com/api/v4/projects/$PROJECT_DEPLOY/pipeline?ref=master"

ID_DEPLOY=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_DEPLOY/pipelines?per_page=1&page=1" | jq '.[] | .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_DEPLOY/pipelines/$ID_DEPLOY/jobs" | jq '.[] | select(.name == "run_deploy") | .id')

curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" \
	"https://gitlab.com/api/v4/projects/$PROJECT_DEPLOY/jobs/$JOB_ID/play"
