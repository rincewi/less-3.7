#!/bin/bash

TOKEN=5_1smDbGkwmaR3MBsovz
PROJECT_ID=19008172

curl --globoff --header "PRIVATE-TOKEN: $TOKEN" \
       	"https://gitlab.com/api/v4/projects/$PROJECT_ID/jobs?scope[]=failed" | jq '.[] | select(.name == "build_prod") | .id' | head -1
